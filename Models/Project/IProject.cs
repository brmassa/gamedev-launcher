namespace GameDevLauncher.Models;

public interface IProject
{
    string Title { get; set; }
    string Path { get; set; }
    IRelease Release { get; set; }
    void Init();
}
