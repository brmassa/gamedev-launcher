namespace GameDevLauncher.Models;

public interface IRelease
{
    string Engine { get; set; }
    string Title { get; set; }
    string Path { get; set; }
    string Version { get; set; }
    void Init();
}
