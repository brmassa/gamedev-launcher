using System.Collections.Generic;

namespace GameDevLauncher.Models;

public class Unity : IEngine
{
    public string Title { get; set; } = "Unity";

    public void Init() { }
}
