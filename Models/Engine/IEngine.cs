using System.Collections.Generic;

namespace GameDevLauncher.Models;

public interface IEngine
{
    string Title { get; set; }
    void Init();
}
