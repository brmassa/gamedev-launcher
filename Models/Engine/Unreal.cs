using System.Collections.Generic;

namespace GameDevLauncher.Models;

public class Unreal : IEngine
{
    public string Title { get; set; } = "Unreal";

    public void Init() { }
}
