using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace GameDevLauncher.Models;

internal class App
{
    private const string Path = "./Cache";
    private const string Extension = ".json";

    public List<IRelease> InstalledReleases { get; set; } = new();
    public List<IProject> InstalledProjects { get; set; } = new();
    public string EngineCurrent { get; set; } = string.Empty;

    private string CachePath => $"{Path}/appData{Extension}";

    private readonly IFileSystem _fileSystem;
    private static readonly SemaphoreSlim semaphoreSlim = new SemaphoreSlim(1, 1);

    public App() : this(new FileSystem())
    {
    }

    public App(IFileSystem fileSystem)
    {
        _fileSystem = fileSystem ?? throw new ArgumentNullException(nameof(fileSystem));
    }

    public static async Task<IEnumerable<IEngine>> LoadEnginesAsync()
    {
        await Task.Delay(millisecondsDelay: 400);
        return new List<IEngine>() { new Flax(), new Godot(), new Unity(), new Unreal(), };
    }

    public static async Task<App?> LoadCachedAsync()
    {
        var app = new App();
        return await app.LoadCachedInternalAsync();
    }

    public async Task SaveAsync()
    {
        if (!Directory.Exists(Path))
        {
            Directory.CreateDirectory(Path);
        }

        await semaphoreSlim.WaitAsync();

        try
        {
            await using var fs = File.Create(CachePath);
            await SaveToStreamAsync(this, fs);
        }
        finally
        {
            semaphoreSlim.Release();
        }
    }


    private async Task<App?> LoadCachedInternalAsync()
    {
        if (!_fileSystem.DirectoryExists(Path))
            return null;

        foreach (var file in _fileSystem.EnumerateFiles(Path))
        {
            if (_fileSystem.GetFileExtension(file) != Extension)
                continue;

            return await LoadFromStreamAsync(file);
        }

        return null;
    }

    private async Task<App?> LoadFromStreamAsync(string path)
    {
        try
        {
            await using var fs = _fileSystem.OpenRead(path);
            return await JsonSerializer.DeserializeAsync<App>(fs).ConfigureAwait(false);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Failed to load app data: {ex.Message}");
            return null;
        }
    }

    private static async Task SaveToStreamAsync(App data, Stream stream)
    {
        await JsonSerializer.SerializeAsync(stream, data).ConfigureAwait(false);
    }
}
