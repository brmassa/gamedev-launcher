using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using System.Threading.Tasks;

public interface IFileSystem
{
    bool DirectoryExists(string path);
    void CreateDirectory(string path);
    string[] EnumerateFiles(string path);
    string GetFileExtension(string path);
    Stream OpenRead(string path);
    Stream OpenWrite(string path);
}

public class FileSystem : IFileSystem
{
    public bool DirectoryExists(string path) => Directory.Exists(path);
    public void CreateDirectory(string path) => Directory.CreateDirectory(path);
    public string[] EnumerateFiles(string path) => Directory.GetFiles(path);
    public string GetFileExtension(string path) => Path.GetExtension(path);
    public Stream OpenRead(string path) => File.OpenRead(path);
    public Stream OpenWrite(string path) => File.OpenWrite(path);
}
