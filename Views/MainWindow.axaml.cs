using GameDevLauncher.ViewModels;
using Avalonia.ReactiveUI;
using Avalonia.Interactivity;
using Avalonia.Controls;
using System;

namespace GameDevLauncher.Views;

internal partial class MainWindow : ReactiveWindow<MainWindowViewModel>
{
    public MainWindow()
    {
        InitializeComponent();
    }

    /// <summary>
    /// Quit the App
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void Quit(object sender, RoutedEventArgs e)
    {
        Close();
    }
}
