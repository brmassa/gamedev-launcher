using Avalonia.Controls;
using Avalonia.Interactivity;

namespace GameDevLauncher.Views;

internal partial class ProjectView : UserControl
{
    public ProjectView()
    {
    }

    /// <summary>
    /// Quit the App
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void OnOpen(object sender, RoutedEventArgs e)
    {
    }
}
