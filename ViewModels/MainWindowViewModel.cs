using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Threading.Tasks;
using ReactiveUI;
using GameDevLauncher.ViewModels;
using Avalonia.Controls;

namespace GameDevLauncher.ViewModels;
internal class MainWindowViewModel : ViewModelBase
{
    private Models.App? data;
    private int engineCurrent;
    private ViewModelBase? selectedSettingsViewModel;
    private ProjectListViewModel? projectListViewModel;
    private BackendListViewModel? backendListViewModel;

    public ObservableCollection<Models.IEngine> Engines { get; } = new();

    public int EngineCurrent
    {
        get => engineCurrent;
        set => this.RaiseAndSetIfChanged(ref engineCurrent, value);
    }

    public ObservableCollection<string> Items { get; } = new ObservableCollection<string>();

    public ViewModelBase? SelectedSettingsViewModel
    {
        get => selectedSettingsViewModel;
        set => this.RaiseAndSetIfChanged(ref selectedSettingsViewModel, value);
    }

    public ReactiveCommand<Unit, Unit> SelectProjectPane { get; }
    public ReactiveCommand<Unit, Unit> SelectBackendPane { get; }

    public MainWindowViewModel()
    {
        // Initialize the commands
        SelectProjectPane = ReactiveCommand.Create(ShowProjectPane);
        SelectBackendPane = ReactiveCommand.Create(ShowBackendPane);

        ShowProjectPane();

        InitializeAsync().ConfigureAwait(false);
    }

    public async Task InitializeAsync()
    {
        data = await Models.App.LoadCachedAsync() ?? new Models.App();
        if (data != null)
        {
            await LoadEnginesAsync();

            this.WhenAnyValue(x => x.EngineCurrent)
                .Skip(1)
                .Select(index => Engines.ElementAtOrDefault(index))
                .Where(engine => engine != null)
                .Do(engine => data.EngineCurrent = engine.Title)
                .Select(_ => Observable.FromAsync(() => data.SaveAsync()))
                .Concat()
                .Subscribe();
        }
    }

    private async Task LoadEnginesAsync()
    {
        var engines = await Models.App.LoadEnginesAsync();
        foreach (var engine in engines)
        {
            Engines.Add(engine);
        }
        if (data != null)
        {
            EngineCurrent = LoadSelectedEngine();
        }
    }

    private int LoadSelectedEngine()
    {
        return Engines.ToList().FindIndex(e => e.Title == data?.EngineCurrent);
    }

    private void ShowProjectPane()
    {
        projectListViewModel ??= new ProjectListViewModel();
        SelectedSettingsViewModel = projectListViewModel;
    }

    private void ShowBackendPane()
    {
        backendListViewModel ??= new BackendListViewModel();
        SelectedSettingsViewModel = backendListViewModel;
    }
}
