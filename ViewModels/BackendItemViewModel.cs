namespace GameDevLauncher.ViewModels;

internal class BackendItemViewModel : ViewModelBase
{
    public string Title { get; set; } = default!;
    public string Path { get; set; } = default!;
}
