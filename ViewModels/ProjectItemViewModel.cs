namespace GameDevLauncher.ViewModels;

internal class ProjectItemViewModel : ViewModelBase
{
    public string Title { get; set; } = string.Empty;
    public string Path { get; set; } = string.Empty;
    public string Backend { get; set; } = string.Empty;
}
